# jardwd.space


A PWA built for [JARD](https://jardwd.space/) - built using [EmberJS](https://ember-cli.com/) v3.8.3



## Installation

* `git clone <repository-url>` this repository
* `cd ember-jardwd`
* `npm install`

## Addons Used

[Ember Paper](https://github.com/miguelcobain/ember-paper)

[PWA on Ember guide](https://blog.201-created.com/pwa-your-ember-app-7ee8242f306e)


## Running / Development

* `ember serve`
* Visit your app at [http://localhost:4200](http://localhost:4200).



### Building

* `ember build` (development)
* `ember build --environment production` (production)

### Deploying

Whatever method it takes to deploy your app.
