'use strict';

module.exports = function(/* environment, appConfig */) {
  // See https://github.com/zonkyio/ember-web-app#documentation for a list of
  // supported properties

  return {
    name: "jard-dev",
    short_name: "JARD",
    description: "JARD web and app development",
    start_url: "/",
    display: "standalone",
    orientation: "landscape",
    background_color: "#F5F5F5",
    theme_color: "#F5F5F5",
    icons: [
     {
        src: "/images/jard-192-icon.min.png",
        sizes: "192x192",
        type: "image/png"
      },
      {
        src: "/images/jard-525-icon.min.png",
        sizes: "525x525",
        type: "image/png"
      }
    ],
    ms: {
      tileColor: '#F5F5F5'
    }
  };
}
