import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | this-is-jard', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:this-is-jard');
    assert.ok(route);
  });
});
